Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ycmd
Source: https://github.com/Valloric/ycmd

Files: *
Copyright: 2011-2015, Google Inc.
License: GPL-3+

Files: cpp/ycm/tests/cmake/FindGMock.cmake
Copyright: 2011, Matej Svec
License: GPL-3+

Files: ycmd/completers/python/hook.py
 ycmd/completers/python/jedi_completer.py
Copyright: 2011-2012, Stephen Sugden <me@stephensugden.com>
License: GPL-3+

Files: ycmd/completers/general/filename_completer.py
 ycmd/completers/general/ultisnips_completer.py
 ycmd/completers/general/general_completer_store.py
Copyright: 2013, Stanislav Golovanov <stgolovanov@gmail.com>
License: GPL-3+

Files: ycmd/completers/cs/hook.py
 ycmd/completers/cs/cs_completer.py
Copyright: 2011-2012, Chiel ten Brinke <ctenbrinke@gmail.com>
License: GPL-3+

Files: cpp/ycm/.ycm_extra_conf.py
Copyright: 2011-2013, Strahinja Val Markovic <val@markovic.io>
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.
License: Unlicense

Files: third_party/JediHTTP/*
Copyright: 2016, Andrea Cedraro <a.cedraro@gmail.com>
License: Apache-2.0

Files: debian/*
Copyright: 2013-2015, Onur Aslan <onur@onur.im>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Unlicense
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any
 means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the
 software to the public domain. We make this dedication for the benefit
 of the public at large and to the detriment of our heirs and
 successors. We intend this dedication to be an overt act of
 relinquishment in perpetuity of all present and future rights to this
 software under copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 For more information, please refer to <http://unlicense.org/>

License: Apache-2.0
 See /usr/share/common-licenses/Apache-2.0.
