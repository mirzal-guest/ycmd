ycmd (0+20180708+git600f54d-1) unstable; urgency=medium

  * New snapshot release (Closes: #893148)
    For now, I disabled the testsuite... Given the current state of
    the package, this isn't a big deal for now.
  * Package moved under the Python umbrella as the maintainer isn't
    active lately.
  * Uses llvm/clang 7 (Closes: #873412)
  * Standards-Version: 4.2.1

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 23 Jul 2018 09:27:18 +0200

ycmd (0+20161219+git486b809-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Run fewer tests on mipsel and mips64el, some fail on Loongson buildds.
    (Closes: #852962)

 -- Adrian Bunk <bunk@debian.org>  Wed, 07 Jun 2017 12:13:33 +0300

ycmd (0+20161219+git486b809-3) UNRELEASED; urgency=medium

  * d/control: Remove ancient X-Python-Version field
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:50:59 +0200

ycmd (0+20161219+git486b809-2) unstable; urgency=low

  * Fix clang version detection in override_dh_link. (Closes: #861483)
  * Try to find tern executable in $PATH and then set PATH_TO_TERN_BINARY
    to ~/.npm-packages/bin/tern. (Closes: #859883)

 -- Onur Aslan <onur@onur.im>  Mon, 08 May 2017 11:23:19 +0300

ycmd (0+20161219+git486b809-1) unstable; urgency=low

  * New upstream release. (Closes: #850144)
  * Bump Standards-Version to 3.9.8 (no changes).
  * Remove third party dependency frozendict from package.
  * Refresh patches for new upstream release.
  * Use updated path for tsserver. (Closes: #841563)
  * Replace google-mock dependency with google-test and use correct
    directory for gmock target. (Closes: #848727)
  * Bumb libclang dependency version to 3.9.
  * Update 01-python-patch.patch to:
    - Fix a test function in JediHTTP to use upper case letter.
    - Remove follow_imports parameter usage from jedi.Script.goto_assignments.
      This parameter is only available in the latest revision of jedi and not
      available in Debian.
  * Remove third_party/frozendict from package
  * Add python-frozendict and python-psutil to Build-Depends.
  * Add 08-bottle.patch to make ycmd compatible with python-bottle.
  * Improve override_dh_auto_clean.
  * Remove installation of check_core_version.py.
  * Remove gmock and gtest files from package.
  * Replace clang includes symbolic link with correct include directory.
    (Closes: #825905)
  * Remove fix permissions from override_dh_install and only make
    /usr/lib/ycmd/ycmd/__main__.py executable.
  * Enable hardening.
  * Remove unused files from debian/copyright.
  * Remove architecture detection for go tests (go tests are excluded
    completely), and reorganize EXCLUDE_TEST_PARAMS in debian/rules.

 -- Onur Aslan <onur@onur.im>  Thu, 26 Jan 2017 12:57:23 +0300

ycmd (0+20160327+gitc3e6904-1) unstable; urgency=low

  * New upstream release. (Closes: #818070)
  * Refresh patches for new upstream release.
  * Add libclang-3.8-dev and remove libclang-dev from Build-Depends.

 -- Onur Aslan <onur@onur.im>  Sat, 09 Apr 2016 12:22:30 +0300

ycmd (0+20160303+git206efaf-1) unstable; urgency=low

  * New upstream release. (Closes: #818070)
  * Bump Standards-Version to 3.9.7 (no changes).
  * Refresh patches for new upstream version.
  * Add copyright info for third_party/JediHTTP.
  * Exclude javascript and rust tests.
  * Install JediHTTP into usr/lib/ycmd/third_party/.
  * Update 01-python-path.patch to remove AddVendorFolderToSysPath
    function from jedihttp. This package is using system packages for
    jedihttp dependencies.
  * Update d/rules get-orig-source to update only required submodules
    instead of cloning repository recursively.
  * Add python3-nose, python3-hamcrest, python3-webtest, python3-jedi,
    python3-waitress, python3-bottle, python3-requests into Build-Depends
    for JediHTTP tests.
  * Update d/rules override_dh_auto_test to run JediHTTP tests.
  * Update d/rules override_dh_install to remove JediHTTP tests.
  * Add 05-tern-support.patch to support local node-tern installation.
  * Add python-future, python-pep8 and python-setuptools into
    Build-Depends.
  * Add python-future into Depends.
  * Update 03-gocode-path.patch to remove unused import to get rid of
    flake8 error.
  * Remove golang dependencies and exclude golang tests. ycmd started using
    godef along with gocode, and godef is not available for Debian. golang
    completion is temporary disabled. Users can still use golang
    completion by installing gocode package and putting godef executable
    into /usr/bin/godef or setting godef_binary_path in
    default_settings.json.
  * Set HOME environment variable to $(CURDIR) when running tests.
  * Update d/rules override_dh_auto_clean to remove .noseids and .cache
    directories generated by nosetests.
  * Use HTTPS URI's for Vcs-Git and Vcs-Browser fields.
  * Add README.Debian to provide additional information about supported
    completion engines.
  * Add 06-omnisharp-path.patch to give optional OmniSharp based C#
    additional support.
  * Replace python-flake8 dependency with flake8. (Closes: #818453)
  * Add 07-shebang.patch to add missing script headers to main executable
    file and check_core_version.py.
  * Remove executable permission from some completer modules.

 -- Onur Aslan <onur@onur.im>  Tue, 29 Mar 2016 14:09:34 +0300

ycmd (0+20151019+git0825f85-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream version.
  * Remove 04-test-basic-string-alias.patch. Fixed in upstream.
  * Update override_dh_auto_clean to remove *.so* files in d/rules.
  * Update override_dh_auto_test to run ./run_tests.py in d/rules.
  * Install CORE_VERSION instead of EXPECTED_CORE_VERSION.
  * Create a symbolic link to clang includes directory in
    /usr/lib/ycmd. (Closes: #800618)
  * Add libclang-common-(CLANG_VER)-dev into recommends.
  * Replace libclang-3.(6|7) dependencies with libclang-dev (>= 3.6) to
    use default clang version provided by llvm-defaults.
  * Add typescript support:
    - Add node-typescript into Build-Depends and Recommends.
    - Add 04-tsserver-path.patch to use node-typescript for typescript
      completion.
    - Remove excluded typescript tests to activate typescript tests in
      d/rules.
  * Update 00-build-system.patch to add support for libclang-3.8-dev.
  * Update 02-generic-ycm-extra-conf-py.patch to simplify
    ycm_extra_conf.py.

 -- Onur Aslan <onur@onur.im>  Mon, 02 Nov 2015 13:40:20 +0300

ycmd (0+20150804+git6f4dbb4-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches for new upstream version.
  * Replace libclang-dev build dependency with
    libclang-3.6-dev | libclang3.7-dev.
  * Remove clang-3.5 support in 00-build-system.patch.
  * Add 04-test-basic-string-alias.patch.
  * Update d/install to install EXPECTED_CORE_VERSION and
    check_core_version.py
  * Update Upstream-Name and Source URL in debian/copyright.
  * Update copyright info and years.

 -- Onur Aslan <onur@onur.im>  Fri, 07 Aug 2015 12:10:08 +0300

ycmd (0+20150616+gitd7a3653-1) unstable; urgency=low

  * Initial release. (Closes: #771330)

 -- Onur Aslan <onur@onur.im>  Mon, 22 Jun 2015 20:06:43 +0300
