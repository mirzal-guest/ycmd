#!/usr/bin/make -f
# -*- makefile -*-

export DEB_BUILD_MAINT_OPTIONS=hardening=+all
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

BUILD_DIR            = $(CURDIR)/ycm_build
GIT_URL              = https://github.com/Valloric/ycmd.git
DEB_SOURCE           = $(shell dpkg-parsechangelog | grep ^Source: | sed -e 's/Source: //')
DEB_VERSION          = $(shell dpkg-parsechangelog | grep ^Version: | sed -e 's/Version: //')
DEB_UPSTREAM_COMMIT  = $(shell echo $(DEB_VERSION) | sed -rn 's/.*git([^-]*).*/\1/p')
DEB_UPSTREAM_VERSION = $(shell echo $(DEB_VERSION) | sed -e 's/-[^-]*$$//')
DEB_INSTALL_DIR      = $(CURDIR)/debian/$(DEB_SOURCE)


# Skip CsCompleter (OmniSharp), javascript (tern), go and rust tests.
# This package doesn't support them out-of-the-box yet.
EXCLUDE_TEST_PARAMS  = --exclude='^cs$$' --exclude='javascript' --exclude='rust' --exclude='go'
LLVM_INCLUDE_DIR     = $(shell llvm-config-7 --includedir)

# AddNearestThirdPartyFoldersToSysPath tests are using third_party/python-future
# directory to test. This directory is removed from ycmd
# package and ycmd is using python-future package. This tests are failing
# because of this reason and needs to be exluded from Debian package.
EXCLUDE_TEST_PARAMS += --exclude='AddNearestThirdPartyFoldersToSysPath_'


EXCLUDE_TEST_PARAMS += --exclude='GetCompletions_UnicodeInLineFilter_test'  # TODO: Need to investigate
EXCLUDE_TEST_PARAMS += --exclude='FromHandlerWithSubservers_test'           # Requires OmniSharp
EXCLUDE_TEST_PARAMS += --exclude='FromWatchdogWithSubservers_test'          # Requires OmniSharp


%:
	dh $@ --with python2 --sourcedirectory=$(CURDIR)/cpp --builddirectory=$(BUILD_DIR) --parallel

override_dh_auto_configure:
	 CXXFLAGS="-I$(LLVM_INCLUDE_DIR)" dh_auto_configure -- -DUSE_SYSTEM_LIBCLANG=ON -DUSE_SYSTEM_BOOST=ON -DUSE_SYSTEM_GMOCK=ON

# override_dh_auto_test:
# ifeq (,$(findstring nocheck,$(DEB_BUILD_OPTIONS)))
# 	# Test JediHTTP
# 	HOME=$(CURDIR) nosetests -w third_party/JediHTTP -v --with-id -d --no-byte-compile --exclude=test_good_gotoassignment_follow_imports
# 	HOME=$(CURDIR) nosetests3 -w third_party/JediHTTP -v --with-id -d --no-byte-compile --exclude=test_good_gotoassignment_follow_imports
# ifeq (,$(filter $(DEB_HOST_ARCH), mipsel mips64el))
# 	# Some tests requires UTF-8 compatible locale
# 	HOME=$(CURDIR) LC_ALL=C.UTF-8 ./run_tests.py --skip-build --no-byte-compile $(EXCLUDE_TEST_PARAMS)
# endif
# endif

override_dh_auto_clean:
	dh_auto_clean
	$(RM) -rfv .noseids third_party/JediHTTP/.noseids .cache
	$(RM) -rfv *.so*
	$(RM) -rfv $(BUILD_DIR)
	$(RM) -rfv Microsoft   # typescript tests generating a Microsoft directory
	find -name '*.pyc' -exec $(RM) -v {} \;  # remove all cached python files

override_dh_install:
	dh_install
	# Remove dot from .ycm_extra_conf.py
	mv $(DEB_INSTALL_DIR)/usr/lib/ycmd/.ycm_extra_conf.py \
	   $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycm_extra_conf.py
	# Remove tests
	$(RM) -rv $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/completers/all/tests \
	          $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/completers/completer_utils_test.py \
	          $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/completers/cpp/tests \
	          $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/completers/general/tests \
	          $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/completers/go/tests \
	          $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/tests \
	          $(DEB_INSTALL_DIR)/usr/lib/ycmd/third_party/JediHTTP/jedihttp/tests \
	          $(DEB_INSTALL_DIR)/usr/include \
	          $(DEB_INSTALL_DIR)/usr/lib/*.a
	# Fix permissions
	chmod 755 $(DEB_INSTALL_DIR)/usr/lib/ycmd/ycmd/__main__.py

override_dh_link:
	$(eval YCMD_CLANG_VER=$(shell objdump -x ycm_core.so | grep -oP 'NEED.*libclang-\K(\d+\.\d+)'))
	echo "misc:Clang-Ver=$(YCMD_CLANG_VER)" >> debian/ycmd.substvars
	dh_link -pycmd usr/lib/clang/$(YCMD_CLANG_VER)/include usr/lib/ycmd/clang_includes/include

get-orig-source:
	TMPDIR=`mktemp -d -t`; \
	git clone $(GIT_URL) $$TMPDIR; \
	cd $$TMPDIR; \
	echo  "Version $(DEB_UPSTREAM_VERSION)"; \
	git checkout $(DEB_UPSTREAM_COMMIT); \
	git submodule update --init third_party/JediHTTP; \
	tar -czvf $(CURDIR)/ycmd.orig.tar.gz .; \
	cd $(CURDIR); \
	echo "Install pkg-perl-tools if the next command fails"; \
	dpt repack.sh --upstream-version $(DEB_UPSTREAM_VERSION) ycmd.orig.tar.gz ; \
	$(RM) ycmd.orig.tar.gz ; \
	$(RM) -r $$TMPDIR
